import React from 'react';
import queryString from 'query-string';
import { useLocation } from 'react-router';
import { useHistory } from 'react-router-dom';
import { Grid, Button } from '@material-ui/core';
import Search from '@material-ui/icons/Search';
import { useForm } from '../../hooks/userForm';
import { getAll } from '../../selectors/getProduct';
import logo from '../../assets/logo.png'


export const SearchBar = ({ setData }) => {

    const browserHistory= useHistory();

    const location = useLocation();
    //console.log( queryString.parse(location.search));
    
    const { q = '' } = queryString.parse(location.search); // le damos string vacio por si no vienen nada en q

    const [ formValues , handleInput ] = useForm( {
        searchText: q
    });

    const { searchText } = formValues;

    const handleSearch = (e) => {
        e.preventDefault();
       
        setData([]);
        browserHistory.push(`/?q=${ searchText }`); 
         
        getAll( searchText ).then(
            (data) => {
                console.log('data',data);
                if(setData)
                    setData(data);
        });
    }


    return (
        <>
        <div >
            <div className="search__main" >
            <form onSubmit={ handleSearch }>
                <Grid container spacing={0} >
                    
                    <Grid item xs={2} align="center" style={{ marginTop:'5px' }}>
                        <img src={logo} alt='descrip'></img>
                    </Grid>
                    <Grid item xs={8} >
                        <input
                            className="search__input"
                            type="text"
                            placeholder="Buscar productos marcas y mas ..."
                            autoComplete="off"
                            name="searchText"
                            value={ searchText }
                            onChange={ handleInput  }/>
                    </Grid>
                    <Grid item xs={2} >
                        <Button 
                            variant="contained"
                            className="search__btn"
                            type="submit"
                            startIcon={<Search />}        
                            />
                    </Grid>
                 
                </Grid>
            </form>
            </div>
            
        </div>
        </>
    )
}
