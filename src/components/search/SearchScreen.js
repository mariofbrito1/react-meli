import React, { useContext, useEffect, useState } from 'react';
import { ListProduct } from '../items/ListProduct';
import { ContextDataMeli } from '../../routers/ContextDataMeli';

export const SearchScreen = ({ history }) => {

    const { MeliData } = useContext( ContextDataMeli );
    const [state, setState] = useState([]);

    useEffect(() => {
        //console.log('MM',MeliData);
        setState(MeliData);
    }, [MeliData ])

    return (
        <>
            <ListProduct data={state} />
        </>
    )
}
