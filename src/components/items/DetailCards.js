import React, { useEffect, useState } from 'react'

import {Button, Card, CardContent, Divider, Grid} from '@material-ui/core';
import { getProduct } from '../../selectors/getProduct';
import { useLocation } from 'react-router';

export const DetailCards = () => {

    const location = useLocation();
    //console.log('milocalizacion', location.pathname);

    const [data, setData] = useState();
    useEffect(() => {
      
        const loc=location.pathname.split(':');
        const id=loc[1];
        console.log('Item id:', id);

        getProduct(id)
            .then((data) => {      
                console.log(data);
                setData(data);
            });
        
           
         // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [ ]);
     
    return (
        <div >
        
        <Grid container >
            <Grid item xs={12} className="detail__main">
                <Card className="detail__main">
                    <Divider />
                    {data && !data.error ?
                    <CardContent>
                        <Grid container spacing={0} >
                        
                            <Grid item xs={8} align="center" >
                                <img src={data.secure_thumbnail} 
                                    alt='descrip'
                                    className='detail__i'
                                    >
                                </img>
                                <h2 className='detail__h2'>Descripción del Producto</h2>
                                <p className='detail__desc'>{data.description}</p>
                            </Grid>
                            <Grid item xs={4}>
                                <p>{data.condition==='new'? 'Nuevo' : 'Usado'}</p>
                                
                                <h4>{data.title}</h4>
                                <h1 className='detail__h'>$ {data.price}</h1>
                                <Button variant="contained"
                                    type="submit"
                                    style={{
                                            padding: "15px",
                                            width:"350px"
                                            }}
                                    color='primary'
                                    >
                                    Comprar
                                </Button>
                            </Grid>
                        </Grid>
                    </CardContent>
                    :
                    <p></p>
                    }
                </Card>
            </Grid>
        </Grid>
        </div>
    )
}
