import React from 'react'
import {Card, CardContent, Divider, Grid } from '@material-ui/core';
import { useHistory } from 'react-router-dom';

export const ProductCards = ({ data }) => {

    const browserHistory= useHistory();
     
    const onDetail = ( ) =>{
        console.log("detail item ID",data.id);
        browserHistory.push(`/items/:${ data.id }`);
    }

    return (
        <>
      
        <Grid   container 
                style={{
                    cursor: 'pointer'            
                }}                 
                onClick= { onDetail }>
                    
            <Grid item xs={12}>
                <Card className='product__main'>
                    <Divider />
                    <CardContent>
                        <Grid container spacing={0} >
                            
                            <Grid item xs={3} align="center" className='product__item'>
                                <img src={data.thumbnail}
                                    className='product__img' 
                                    alt='descrip'>
                                </img>
                            </Grid>
                            <Grid item xs={6} >
                                    <h2 style={{color:"#535353"}}>$ {data.price}</h2>
                                    <h4 style={{color:"#7a7a7a"}}>{data.id}</h4>
                                    <h4 style={{color:"#7a7a7a"}}>{data.title}</h4>
                                    <h4 style={{color:"#7a7a7a"}}>Autor: {data.author.name}  {data.author.lastname}</h4>
                            </Grid>
                            <Grid item xs={3}>
                                    <p style={{color:"#535353", marginTop:"80px"}}> {data.address.state_name}</p>
                                    
                            </Grid>
                    
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
        </>
    )
}
