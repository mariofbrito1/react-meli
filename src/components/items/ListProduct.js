import React from 'react'
import { ProductCards } from './ProductCards'

export const ListProduct = ({ data }) => {
    
    return (
        <div className="col-12" >
                {data &&
                    data.map( p => (
                        <ProductCards key={p.id} data={ p } />
                    ))
                }
        </div>
    )
}
