
import ApiService from "../services/meli.service";

export const getProduct = async( data = '' ) => {

    const res = await ApiService.getByProductId(data);  
    const p = res.data;
    if(!p){
        return [];
    }else{
        return p;
    }
} 

export const getAll = async( data = '' ) => {
     
    const res = await ApiService.searchQ(data);  
    const p = res.data;
    if(!p){
        return [];
    }else{
        return p;
    }
} 