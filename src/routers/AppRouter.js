import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { SearchScreen } from '../components/search/SearchScreen';
import { DetailCards } from '../components/items/DetailCards';
import { SearchBar } from '../components/search/SearchBar';
import { ContextDataMeli } from './ContextDataMeli';


export const AppRouter = () => {

    /*
    * Usamos el contextProvider global para servir 
    * los componentes mas internos de la app "ContextDataMeli"
    */
    
    const [state, setData] = useState([]);

    return (
        
        <Router>
          <ContextDataMeli.Provider value={{MeliData: state}}>
            <SearchBar setData={setData}/>
              <Switch>
                <Route exact path="/" 
                  component={ SearchScreen }
                  />
                <Route  path="/items/" 
                  component={ DetailCards }
                  />
                  
              </Switch>
          </ContextDataMeli.Provider>
      </Router>

    )
}
