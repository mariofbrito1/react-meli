import http from "../http-common";

class MeliDataService {

  searchQ(q) {
    return http.get(`/api/items?q=:${q}`);
  }

  getByProductId(id) {
    return http.get(`/api/items/${id}`);
  }
}

export default new MeliDataService();