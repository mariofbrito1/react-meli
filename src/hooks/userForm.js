import { useState } from 'react'

export const useForm = ( initialState = {} ) => {
    
    const [values, setValues] = useState(initialState);

    const reset = () => {
        setValues( initialState );
    }

    const handleInputChange = ({ target }) => {
        setValues({
            ...values,
            [ target.name ] : target.value  
        });
    }

    // el retorno puede ser de manera arbitraria puede ser valor en este caso un array
    // el primer valor es el estado del formulario el segundo es para cambiar los valores del formulario
    return [ values , handleInputChange, reset];
}
